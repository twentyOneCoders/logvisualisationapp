package logvisualisationapp.sources;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.Collections.*;

public enum Filters implements Filter {

    CHANNEL_WITH_STATE(singletonList(1), singletonList(2), singletonList(Arrays.asList(3)), Arrays.asList(4, 7), singletonList(5), singletonList(6)),
    BRIDGE_AST_EVENT(singletonList(1), singletonList(2), Arrays.asList(Arrays.asList(4), Arrays.asList(5)), singletonList(3), null, singletonList(6));

    private static Pattern patternChannel = Pattern.compile("(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}).*" +
            "\\[service.tms.tapi.TapiHandler] Received msg: \\[ChannelStateChangeMsg\\(ChannelWithState.(Channel): " +
            "channel=\\[(.*)] state=\\[(\\w{2,7})] direction=\\[(?:\\w{5,6})] src=\\[(\\w{11})] dst=\\[(?:.*)] " +
            "uid=\\[(?:.*)] cidname=\\[(?:.*)] nodename=\\[(\\w+)] queuePriority=\\[(?:.*)] dialTo=\\[None],(\\w{7,11})\\)\\)]");

    private static Pattern patternBridge = Pattern.compile("(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}).*" +
            "\\[service.tms.astevents.AstEvents] Received msg: \\[(BridgeAstEvent)\\((.*) " +
            "channel1=\\[(.*)] channel2=\\[(.*)] nodeName=\\[(\\w+)].*");

    private List<Integer> timestamp;
    private List<Integer> typeOfEvent;
    private List<List<Integer>> keyGroups;
    private List<Integer> eventGroups;
    private List<Integer> nodename;
    private List<Integer> addInf;

    Filters(List<Integer> timestamp, List<Integer> typeOfEvent, List<List<Integer>> keyGroups,
            List<Integer> eventGroups, List<Integer> addInf, List<Integer> nodename) {
        this.timestamp = timestamp;
        this.typeOfEvent = typeOfEvent;
        this.keyGroups = keyGroups;
        this.eventGroups = eventGroups;
        this.addInf = addInf;
        this.nodename = nodename;
    }

    public static Pattern getPatternChannel() {
        return patternChannel;
    }

    public static Pattern getPatternBridge() {
        return patternBridge;
    }

    @Override
    public List<Integer> getTimestamp() {
        return timestamp;
    }

    @Override
    public List<Integer> getTypeOfEvent() {
        return typeOfEvent;
    }

    @Override
    public List<List<Integer>> getKeyGroups() { return keyGroups; }

    @Override
    public List<Integer> getEventGroups() { return eventGroups; }

    @Override
    public List<Integer> getNodename() {
        return nodename;
    }

    @Override
    public List<Integer> getAddInf() { return addInf; }

    @Override
    public boolean isAddInfExist() {
        return addInf != null;
    }
}
