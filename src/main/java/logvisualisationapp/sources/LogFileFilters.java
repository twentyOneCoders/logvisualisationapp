package logvisualisationapp.sources;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logvisualisationapp.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class LogFileFilters implements Source {

    private static Logger logger = LoggerFactory.getLogger(LogFileFilters.class);
    private List<Filters> filters;
    private List<String> logList = new ArrayList<>();

    public LogFileFilters(List<Filters> filters) {
        this.filters = filters;
    }

    /**
     * Сохраняем логи в лист.
     *
     * @param file
     * @return
     * @throws FileNotFoundException
     */
    private void saveFileInList(File file) throws FileNotFoundException {

        //Объект для чтения файла в буфер
        try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
            //В цикле построчно считываем файл
            String s;
            while ((s = in.readLine()) != null) {
                logList.add(s);
            }
        } catch(IOException e) {
            logger.error("Log файл не сохранен в List.", e);
            throw new RuntimeException(e);
        }

        logger.info("Лог сохранен в ArrayList.");
    }

    /**
     * Ищем совпадения в листе логов по заданным паттернам и записываем нужные логи в новый лист.
     *
     * @param file
     * @return
     */
    public ObservableList<Log> findMatches(File file) {

        try {
            saveFileInList(file);
        } catch (FileNotFoundException e) {
            logger.error("Log файл не обработан.", e);
        }

        ObservableList<Log> logData = FXCollections.observableArrayList();
        Matcher matcher;

        for (String logForMatchByFilters : logList) {
            for (Filter filter : filters) {

                if (filter.getAddInf() != null) {
                    matcher = Filters.getPatternChannel().matcher(logForMatchByFilters);
                }
                else {
                    matcher = Filters.getPatternBridge().matcher(logForMatchByFilters);
                }

                if (matcher.matches()) {

                    Log oneLog = new Log();

                    oneLog.setTimestamp(filter.getTimestamp()
                            .stream()
                            .map(matcher::group)
                            .collect(Collectors.joining(",")));

                    oneLog.setTypeOfEvent(filter.getTypeOfEvent()
                            .stream()
                            .map(matcher::group)
                            .collect(Collectors.joining(",")));

                    oneLog.setKey(getKeyList(filter, matcher));

                    oneLog.setState(filter.getEventGroups()
                            .stream()
                            .map(matcher::group)
                            .collect(Collectors.joining(",")));

                    oneLog.setNodename(filter.getNodename()
                            .stream()
                            .map(matcher::group)
                            .collect(Collectors.joining(",")));

                    if (filter.isAddInfExist()) {
                        oneLog.setAddInf(filter.getAddInf()
                                .stream()
                                .map(matcher::group)
                                .collect(Collectors.joining(",")));
                    }
                    logData.add(oneLog);

                }
            }
        }
        logger.info(logData.size() + " логов, попадающих под фильтры, записаны в ObservableList.");
        return logData;
    }

    /**
     * Метод по формированию листа связанных ключей.
     *
     * @param filter
     * @param matcher
     * @return
     */
    private List<String> getKeyList(Filter filter, Matcher matcher) {

        List<String> listKeys = new ArrayList<>();
        List<List<Integer>> keyList = filter.getKeyGroups();

        for (List<Integer> oneList : keyList) {

            List<String> oneListStrings = oneList
                    .stream()
                    .map(matcher::group)
                    .collect(Collectors.toList());

            String keys = String.join("\n", oneListStrings);
            listKeys.add(keys);
        }

        return listKeys;
    }

}
