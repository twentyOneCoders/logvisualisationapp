package logvisualisationapp.sources;

import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import logvisualisationapp.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogFileText extends LogFileFilters {

    private static Logger logger = LoggerFactory.getLogger(LogFileFilters.class);
    private final Set<String> keysFromTextField;
    private Map<Integer, Log> logsFindByText = new TreeMap<>();

    public LogFileText(List<Filters> filters, Set<String> keysFromTextField) {
        super(filters);
        this.keysFromTextField = keysFromTextField;
    }

    /**
     * Метод для поиска логов по искомой строке.
     *
     * @param file
     * @return
     */
    @Override
    public ObservableList<Log> findMatches(File file) {
        int newKeysCount = keysFromTextField.size();
        int oldKeysCount = 0;
        String addInf = null;
        Set<String> newKeys = keysFromTextField;
        ObservableList<Log> logDataForTable = super.findMatches(file);

        if (!(keysFromTextField.size() == 1 && (keysFromTextField.contains("")))) {

            for (String key : keysFromTextField) {
                Matcher matcher = Pattern.compile("7\\w{10}").matcher(key);
                if (matcher.matches()) {
                    addInf = key;
                }
            }

            while (oldKeysCount != newKeysCount) {
                oldKeysCount = newKeysCount;
                newKeys = recursiveSearch(logDataForTable, newKeys, addInf);
                newKeysCount = newKeys.size();
            }

            logDataForTable.clear();

            for (Map.Entry entry : logsFindByText.entrySet()) {
                Log log = (Log) entry.getValue();
                logDataForTable.add(log);
            }

            if (logDataForTable.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("No Matches Found");
                alert.setHeaderText(null);
                alert.setContentText("No matches were found for the search string.");
                alert.showAndWait();
            }
            logger.info(logDataForTable.size() + " логов, попадающих под искомую строку, записаны в ObservableList.");
            return logDataForTable;
        }

        return logDataForTable;
    }

    /**
     * Ищем совпадения в листе логов, отфильтрованных по паттернам,
     * по искомой строке и записываем нужные логи в HashMap.
     *
     * @param logData
     * @param previousKeys
     * @param addInf
     * @return
     */
    private Set<String> recursiveSearch(ObservableList<Log> logData, Set<String> previousKeys, String addInf) {

        Set<String> newFindKeys = new HashSet<>();

        for (int i = 0; i < logData.size(); i++) {
            Log logForMatchToFindNewLog = logData.get(i);
            List<String> keysFromOneLog = logForMatchToFindNewLog.getKey();
            String addInfFromLog = "";

            if (logForMatchToFindNewLog.isAddInfExist(logForMatchToFindNewLog.getAddInf())) {
                addInfFromLog = logForMatchToFindNewLog.getAddInf();
            }

            for (String keyFromLog : keysFromOneLog) {

                for (String key : previousKeys) {

                    if ((addInfFromLog.equals(addInf) || keyFromLog.contains(key)) && !logsFindByText.containsKey(i)) {
                        newFindKeys.addAll(keysFromOneLog);
                        logsFindByText.put(i, logForMatchToFindNewLog);
                    }
                }
            }
        }
        return newFindKeys;
    }

}
