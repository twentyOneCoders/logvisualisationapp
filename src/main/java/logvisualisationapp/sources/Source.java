package logvisualisationapp.sources;

import javafx.collections.ObservableList;
import logvisualisationapp.Log;

import java.io.File;

public interface Source {
    ObservableList<Log> findMatches(File file);
}
