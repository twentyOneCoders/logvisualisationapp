package logvisualisationapp.sources;

import java.util.List;

public interface Filter {
    List<Integer> getTimestamp();
    List<Integer> getTypeOfEvent();
    List<List<Integer>> getKeyGroups();
    List<Integer> getEventGroups();
    List<Integer> getNodename();
    List<Integer> getAddInf();
    boolean isAddInfExist();
}
