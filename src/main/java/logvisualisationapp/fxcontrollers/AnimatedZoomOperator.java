package logvisualisationapp.fxcontrollers;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Класс для работы с масштабированием canvas.
 */
class AnimatedZoomOperator {

    private Timeline timeline;

    // создаем объект с частотой кадров 60 в сек
    AnimatedZoomOperator() { this.timeline = new Timeline(60); }

    void zoom(Node node, double factor, double x, double y) {
        double MAX_SCALE = 1.5;
        double MIN_SCALE = 0.5;
        // определяем масштаб
        double oldScale = node.getScaleX();
        double scale = oldScale * factor;
        double f = (scale / oldScale) - 1;

        if((scale <= MAX_SCALE) && (scale >= MIN_SCALE)) {
            // определяем смещение, на которое нам нужно будет переместить узел
            Bounds bounds = node.localToScene(node.getBoundsInLocal());
            double dx = (x - (bounds.getWidth() / 2 + bounds.getMinX()));
            double dy = (y - (bounds.getHeight() / 2 + bounds.getMinY()));

            // таймлайн, которая масштабирует и перемещает узел
            timeline.getKeyFrames().clear();
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.millis(200), new KeyValue(node.translateXProperty(), node.getTranslateX() - f * dx)),
                    new KeyFrame(Duration.millis(200), new KeyValue(node.translateYProperty(), node.getTranslateY() - f * dy)),
                    new KeyFrame(Duration.millis(200), new KeyValue(node.scaleXProperty(), scale)),
                    new KeyFrame(Duration.millis(200), new KeyValue(node.scaleYProperty(), scale))
            );
            timeline.play();
        }
    }

}
