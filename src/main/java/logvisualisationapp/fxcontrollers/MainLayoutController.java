package logvisualisationapp.fxcontrollers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import logvisualisationapp.Log;
import logvisualisationapp.Main;
import logvisualisationapp.sources.Filters;
import logvisualisationapp.sources.LogFileText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class MainLayoutController {

    @FXML
    private TableView<Log> logTable;
    @FXML
    private TableColumn<Log, String> timestamp;
    @FXML
    private TableColumn<Log, String> typeOfEvent;
    @FXML
    private TableColumn<Log, String> keys;
    @FXML
    private TableColumn<Log, String> state;
    @FXML
    private TableColumn<Log, String> nodename;
    @FXML
    private TableColumn<Log, String> addInf;
    @FXML
    private Canvas canvas;
    @FXML
    private TextField textField;
    @FXML
    private Button buttonGo;
    @FXML
    private Button buttonCancel;

    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private Main main;
    private Parser parser = new Parser();
    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    private Future<ObservableList<Log>> future;
    private Set<String> logEventsInCanvas = new HashSet<>();
    private int countOfEvents = 0;
    private int countOfLogEvents = 0;
    private Map<String, Integer> numberOfLogDiagram = new HashMap<>();
    private static final String STYLE_OF_TEXT = "Calibri";
    private int[] paramOfChannelText = new int[]{25, 30};
    private int[] paramOfTypeOfEventText = new int[]{100, 20};
    private int[][] paramOfTimestampText = new int[][]{{0}, {20, 60}};
    private int[][] paramOfStateText = new int[][]{{195, 250}, {20, 60}};
    private int[][] paramOfTranslate = new int[][]{{100, 250}, {0}};
    private int[][] paramOfRoundRect = new int[][]{{20, 0}, {230, 50}, {20}};
    private int[][] paramOfOval = new int[][]{{190, 250}, {60}, {70, 30}};
    private int[][] paramOfVerticalStrokeLine = new int[][]{{130, 50}, {130, 60}};

    @FXML
    private void initialize() {
        // Инициализация таблицы с 5 столбцами.
        timestamp.setCellValueFactory(cellData -> cellData.getValue().timestampProperty());
        typeOfEvent.setCellValueFactory(cellData -> cellData.getValue().typeOfEventProperty());
        keys.setCellValueFactory(cellData ->
        {
            List<StringProperty> list = cellData.getValue().keyProperty();
            String s = list.stream().map(StringProperty::getValue).collect(Collectors.joining(";\n"));
            return new SimpleStringProperty(s);
        });
        state.setCellValueFactory(cellData -> cellData.getValue().stateProperty());
        nodename.setCellValueFactory(cellData -> cellData.getValue().nodenameProperty());
        addInf.setCellValueFactory(cellData -> cellData.getValue().addInfProperty());

        addListenerForTable();

        setOnScroll();
    }

    public void setMain(Main main) {
        this.main = main;
    }

    /**
     * При нажатие на кнопку "Down" происходит отрисовка нового
     * лога из таблицы в Canvas.
     */
    private void addListenerForTable() {

        logTable.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue != null) {
                drawLog();
            }
        }));
    }

    /**
     * Метод по отрисовке лога.
     */
    private void drawLog() {

        if ((canvas.getWidth() < 7800) && (canvas.getHeight() < 8000)) {

            int sizeOfStateText = 14;
            int sizeOfTimestampText = 12;
            Log logInTable = logTable.getSelectionModel().getSelectedItem();
            String timestamp = logInTable.getTimestamp();
            List<String> keyList = logInTable.getKey();
            String state = logInTable.getState();
            GraphicsContext gc = canvas.getGraphicsContext2D();

            String logEvent = timestamp + " " + keyList + " " + state;

            if (!logEventsInCanvas.contains(logEvent)) {

                logEventsInCanvas.add(logEvent);
                countOfEvents++;

                for (String key : keyList) {

                    double canvasHeight = canvas.getHeight();
                    int extraHeight = 40;
                    canvas.setHeight(canvasHeight + extraHeight);

                    gc.setFill(Color.BLACK);
                    gc.setFont(new Font(STYLE_OF_TEXT, sizeOfTimestampText));
                    gc.fillText(timestamp, paramOfTimestampText[0][0],
                            paramOfTimestampText[1][0] + countOfEvents * paramOfTimestampText[1][1]);

                    isLogEventExistInCanvas(key, logInTable);

                    gc.save();
                    gc.translate(paramOfTranslate[0][0] + numberOfLogDiagram.get(key) * paramOfTranslate[0][1],
                            paramOfTranslate[1][0]);
                    gc.strokeLine(paramOfVerticalStrokeLine[0][0], paramOfVerticalStrokeLine[0][1],
                            paramOfVerticalStrokeLine[1][0], countOfEvents * paramOfVerticalStrokeLine[1][1]);
                    gc.restore();

                    getColorByState(logInTable, key);

                    gc.fillOval(paramOfOval[0][0] + numberOfLogDiagram.get(key) * paramOfOval[0][1],
                            countOfEvents * paramOfOval[1][0],
                            paramOfOval[2][0], paramOfOval[2][1]);

                    gc.setFill(Color.BLACK);
                    gc.setFont(new Font(STYLE_OF_TEXT, sizeOfStateText));
                    gc.fillText(state, paramOfStateText[0][0] + numberOfLogDiagram.get(key) * paramOfStateText[0][1],
                            paramOfStateText[1][0] + countOfEvents * paramOfStateText[1][1]);
                }
            }

        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(main.getMainStage());
            alert.setTitle("Canvas is crowded");
            alert.setHeaderText("Canvas is crowded");
            alert.setContentText("Canvas already has the maximum size.");
            alert.showAndWait();
        }
    }

    /**
     * Метод проверяющий отрисован ли текущий канал в кансаве.
     *
     * @param key
     * @param logInTable
     */
    private void isLogEventExistInCanvas(String key, Log logInTable) {

        int extraWidth = 240;
        double canvasWidth = canvas.getWidth();

        if (!numberOfLogDiagram.containsKey(key)) {
            numberOfLogDiagram.put(key, countOfLogEvents);
            countOfLogEvents++;
            canvas.setWidth(canvasWidth + extraWidth);

            drawLogEvent(logInTable, key);
        }
    }

    /**
     * Метод по созданию объекта для масштабирования канваса.
     */
    private void setOnScroll() {

        canvas.setOnScroll(event -> {
            if (event.isControlDown()) {
                AnimatedZoomOperator zoomOperator = new AnimatedZoomOperator();
                double zoomFactor = 1.5;
                // уменьшение
                if (event.getDeltaY() <= 0) {
                    zoomFactor = 1 / zoomFactor;
                }
                zoomOperator.zoom(canvas, zoomFactor, event.getSceneX(), event.getSceneY());
            }
        });
    }

    /**
     * Метод для получения нужного цвета для отрисовки состояния канала.
     *
     * @param logInTable
     */
    private void getColorByState(Log logInTable, String key) {
        String states = logInTable.getState();
        String[] statesArray = states.split(",");
        String state = statesArray[0];
        GraphicsContext gc = canvas.getGraphicsContext2D();

        switch (state) {
            case "RINGING":
                gc.setFill(Color.FIREBRICK);
                break;
            case "UP":
                gc.setFill(Color.BISQUE);
                break;
            case "HANGUP":
                gc.setFill(Color.LIGHTGRAY);
                drawLogEvent(logInTable, key);
                break;
            case "Link":
                gc.setFill(Color.AQUA);
                break;
            case "Unlink":
                gc.setFill(Color.LIGHTGRAY);
                break;
            default:
                break;
        }
    }

    /**
     * Метод для отрисовки канала.
     *
     * @param logInTable
     */
    private void drawLogEvent(Log logInTable, String key) {
        int sizeOfTypeOfEventText = 16;
        int sizeOfChannelText = 10;
        String typeOfEvent = logInTable.getTypeOfEvent();
        String state = logInTable.getState();
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.save();
        gc.translate(paramOfTranslate[0][0] + numberOfLogDiagram.get(key) * paramOfTranslate[0][1],
                paramOfTranslate[1][0]);

        if ("HANGUP".equals(state)) {
            gc.setFill(Color.LIGHTGRAY);
        } else {
            gc.setFill(Color.LIGHTSEAGREEN);
        }

        gc.fillRoundRect(paramOfRoundRect[0][0], paramOfRoundRect[0][1],
                paramOfRoundRect[1][0], paramOfRoundRect[1][1],
                paramOfRoundRect[2][0], paramOfRoundRect[2][0]);
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(STYLE_OF_TEXT, sizeOfChannelText));
        gc.fillText(key, paramOfChannelText[0], paramOfChannelText[1]);
        gc.setFont(new Font(STYLE_OF_TEXT, sizeOfTypeOfEventText));
        gc.fillText(typeOfEvent, paramOfTypeOfEventText[0], paramOfTypeOfEventText[1]);
        gc.restore();
    }

    /**
     * Обработчик кнопки Go. По нажатию вызывает методы для старта и выполнения нового потока.
     */
    public void pressOnButtonGo() throws ExecutionException, InterruptedException {
        clear();

        parser.runFuture();

        buttonGo.setDisable(true);
        buttonCancel.setDisable(false);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Parsing was started.");
        alert.setHeaderText(null);
        alert.setContentText("Parsing is in the process.\n\nWait until all events are found.");
        alert.showAndWait();
    }

    /**
     * Обработчик кнопки Cancel. По нажатию вызывает метод по выполнению потока для его отмены.
     */
    public void pressOnButtonCancel() {
        clear();

        future.cancel(true);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Parsing canceled.");
        alert.setHeaderText(null);
        alert.setContentText("Parsing canceled.\n\nIf you wants to parsing chosen file then\njust click \"Go\" button.");
        alert.showAndWait();

        buttonGo.setDisable(false);
        buttonCancel.setDisable(true);
    }

    /**
     * Метод по очищающий канвас, таблицу и переменные, связанные с отрисовкой лог-событий.
     */
    private void clear() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        canvas.setHeight(300);
        canvas.setWidth(500);
        numberOfLogDiagram.clear();
        countOfLogEvents = 0;
        countOfEvents = 0;
        logTable.getItems().clear();
    }

    private class Parser {

        Callable<ObservableList<Log>> parsingFile = () -> {

            List<Filters> filters = new ArrayList<>();
            filters.add(Filters.CHANNEL_WITH_STATE);
            filters.add(Filters.BRIDGE_AST_EVENT);

            String stringFromTextField = textField.getText();
            String[] keys = stringFromTextField.split(".\\|");
            Set<String> keysFromTextField = new HashSet<>();
            keysFromTextField.addAll(Arrays.asList(keys));

            LogFileText logFileText = new LogFileText(filters, keysFromTextField);

            File file = main.getPersonFilePath();
            return logFileText.findMatches(file);
        };

        /**
         * Добавляет в таблицу данные из наблюдаемого списка.
         */
        Runnable fillTable = () -> {
            if (!future.isCancelled()) {
                try {
                    logTable.setItems(future.get());
                    logger.info("Логи из ObservableList добавлены в таблицу.");
                    buttonGo.setDisable(false);
                } catch (InterruptedException | ExecutionException e) {
                    logger.info("Поток прерван.", e);
                } catch (CancellationException e) {
                    logger.error("Парсинг отменен.", e);
                }
            }
        };

        private void runFuture() throws ExecutionException, InterruptedException {
            future = executorService.submit(parsingFile);
            executorService.submit(fillTable);
        }

    }

}
