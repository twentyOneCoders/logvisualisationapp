package logvisualisationapp.fxcontrollers;

import javafx.fxml.FXML;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import logvisualisationapp.Main;

import java.io.File;
import java.util.StringJoiner;
import java.util.prefs.Preferences;

public class MenuLayoutController {

    private Main main;
    private Stage mainStage;

    public void setMain(Main main) {
        this.main = main;
    }
    public void setMainStage(Stage mainStage) { this.mainStage = mainStage; }

    /**
     * Открывает FileChooser, чтобы пользователь имел возможность
     * выбрать логгер для его дальнейшей обработки.
     */
    @FXML
    private void handleOpen() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String filePath = prefs.get("filePath", null);
        File file = main.getPersonFilePath();
        FileChooser fileChooser = new FileChooser();

        if (file != null) {
            String[] path = filePath.split("/");
            StringJoiner stringJoiner = new StringJoiner("/");
            for (int i = 0; i < path.length - 1; i++) {
                stringJoiner.add(path[i]);
            }
            String newFilePath = stringJoiner.toString();
            fileChooser.setInitialDirectory(new File(newFilePath));

            // Обновление заглавия сцены.
            mainStage.setTitle("LogVisualisationApp - " + file.getName());
        } else {
            // Обновление заглавия сцены.
            mainStage.setTitle("LogVisualisationApp");
        }
        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "Log files (*.log)", "*.log");
        fileChooser.getExtensionFilters().add(extFilter);

        // Показываем диалог загрузки файла
        file = fileChooser.showOpenDialog(main.getMainStage());
        main.setPersonFilePath(file);

    }

    /**
     * Закрывает приложение.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }

}
