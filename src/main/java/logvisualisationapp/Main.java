package logvisualisationapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import logvisualisationapp.fxcontrollers.MainLayoutController;
import logvisualisationapp.fxcontrollers.MenuLayoutController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.prefs.Preferences;

public class Main extends Application {

    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private Stage mainStage;
    private BorderPane menuLayout;

    public static void main(String[] args) {
        launch(args);
    }

    public Stage getMainStage() { return this.mainStage; }

    public void start(Stage mainStage) {
        this.mainStage = mainStage;
        this.mainStage.setTitle("LogVisualisationApp");
        this.initMenuLayout();
    }

    /**
     * Инициализируем макет с меню.
     */
    private void initMenuLayout() {
        try {
            // Загружаем макет c меню из fxml файла.
            FXMLLoader loader = new FXMLLoader();
            URL resource = ClassLoader.getSystemResource("MenuLayout.fxml");
            loader.setLocation(resource);
            this.menuLayout = (BorderPane) loader.load();

            // Отображаем сцену, содержащую макет c меню.
            Scene scene = new Scene(this.menuLayout);
            this.mainStage.setScene(scene);

            // Даём контроллеру доступ к главному приложению.
            MenuLayoutController controller = loader.getController();
            controller.setMain(this);
            controller.setMainStage(mainStage);

            this.mainStage.show();
        } catch (IOException e) {
            logger.error("Не отображен макет содержащий меню.", e);
        }

        // Пытается загрузить последний открытый файл.
        File file = getPersonFilePath();
        if (file != null) {
            mainStage.setTitle("LogVisualisationApp - " + file.getName());
            showMainLayout();
        }
    }

    /**
     * Показываем основной макет приложения.
     */
    private void showMainLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            URL resource = ClassLoader.getSystemResource("MainLayout.fxml");
            loader.setLocation(resource);
            AnchorPane mainLayout = loader.load();

            // Помещаем сведения о логах в центр главного макета.
            this.menuLayout.setCenter(mainLayout);

            MainLayoutController controller = loader.getController();
            controller.setMain(this);
        } catch (IOException e) {
            logger.error("Не отображен основной макет приложения.", e);
        }
    }

    /**
     * Возвращает preference файла, то есть, последний открытый файл.
     * Если preference не был найден, то возвращается null.
     *
     * @return
     */
    public File getPersonFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    /**
     * Задаёт путь текущему загруженному файлу.
     *
     * @param file - файл или null, чтобы удалить путь
     */
    public void setPersonFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());

            // Обновление заглавия сцены.
            mainStage.setTitle("LogVisualisationApp - " + file.getName());
        } else {
            prefs.remove("filePath");

            // Обновление заглавия сцены.
            mainStage.setTitle("LogVisualisationApp");
        }
    }
}
