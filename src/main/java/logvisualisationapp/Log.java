package logvisualisationapp;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

public class Log {

    private StringProperty timestamp;
    private StringProperty typeOfEvent;
    private List<StringProperty> key;
    private StringProperty state;
    private StringProperty nodename;
    private StringProperty addInf;

    @Override
    public String toString() {
        return "Log{" +
                "timestamp=" + timestamp.getValue() +
                ", typeOfEvent=" + typeOfEvent.getValue() +
                ", state=" + state.getValue() +
                ", key=" + key +
                '}';
    }

    public Log () {
            this.timestamp = new SimpleStringProperty();
            this.typeOfEvent = new SimpleStringProperty();
            key = new ArrayList<>();
            this.state = new SimpleStringProperty();
            this.nodename = new SimpleStringProperty();
            this.addInf = new SimpleStringProperty();
    }

    public String getTimestamp() {
        return timestamp.get();
    }

    public void setTimestamp(String timestamp) { this.timestamp.set(timestamp); }

    public StringProperty timestampProperty() {
        return timestamp;
    }

    public String getTypeOfEvent() {
        return typeOfEvent.get();
    }

    public void setTypeOfEvent(String typeOfEvent) {
        this.typeOfEvent.set(typeOfEvent);
    }

    public StringProperty typeOfEventProperty() { return typeOfEvent; }

    public List<String> getKey() {
        List<String> keys = new ArrayList<>();
        for(StringProperty key : key) {
            keys.add(key.get());
        }
        return keys;
    }

    public void setKey(List<String> keys) {
        for(int i = 0; this.key.size() < keys.size(); i++) {
            key.add(new SimpleStringProperty());
            key.get(i).set(keys.get(i));
        }
    }

    public List<StringProperty> keyProperty() {
        return key;
    }

    public String getState() { return state.get(); }

    public void setState(String state) { this.state.set(state); }

    public StringProperty stateProperty() {
        return state;
    }

    public String getNodename() {
        return nodename.get();
    }

    public void setNodename(String nodename) {
        this.nodename.set(nodename);
    }

    public StringProperty nodenameProperty() {
        return nodename;
    }

    public String getAddInf() { return addInf.get(); }

    public StringProperty addInfProperty() { return addInf; }

    public void setAddInf(String addInf) { this.addInf.set(addInf); }

    public boolean isAddInfExist(String addInf) {
        return addInf != null;
    }
}
